if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 	}else{
    	document.querySelector('body').classList.add('desktop');
};

AOS.init({
    disable: 'phone'
});

if(document.querySelector('#background_video')){
	(function () {

	  var bv = new Bideo();
	  bv.init({
	    // Video element
	    videoEl: document.querySelector('#background_video'),

	    // Container element
	    container: document.querySelector('video_bg_section'),

	    // Resize
	    resize: true,

	    // autoplay: false,

	    // isMobile: window.matchMedia('(max-width: 767px)').matches,
	    isMobile: !document.body.classList.contains('desktop'),

	    playButton: document.querySelector('#play'),
	    pauseButton: document.querySelector('#pause'),

	    // What to do once video loads (initial frame)
	    onLoad: function () {
	      document.querySelector('#video_cover').style.display = 'none';
	    }
	  });
	}());
};

var items = document.querySelectorAll('.two_columns_list > li');
var arrayItems = Array.prototype.slice.call(items);

var z=0;
var l=items.length;
var g = Math.ceil(l/2);
var k = Math.ceil(l/2);
for (var i = 0; i < k; i++) {
arrayItems[z].className+=('ASDD')+z;
z++;
};
var t=0;
for (var d = z; d < l; d++) {
arrayItems[g].className+=('ASDD')+t;
g++;
t++;
};

var b = document.querySelector(".two_columns_list");

b.addEventListener("click hover", function(){
  // Cancel the timer
  clearTimeout(timer);
});

document.addEventListener("DOMContentLoaded", function() {
	for (var i = 0; i < k; i++) {
		matchHeight('.two_columns_list > li.'+('ASDD')+i);
	};
	document.querySelector('html').classList.add('loaded');
});
window.addEventListener('resize', function() {
	for (var i = 0; i < k; i++) {
		matchHeight('.two_columns_list > li.'+('ASDD')+i);
	};
});

window.onscroll = function() {
	// if(!document.querySelector('#mouse').classList.contains('invisible'))

    if (window.pageYOffset > 250) {if(!document.querySelector('#mouse').classList.contains('invisible')) document.querySelector('#mouse').className+=(' invisible')}
    	else document.querySelector('#mouse').className = document.querySelector('#mouse').className.replace(/\binvisible\b/g, '') + ' '
};
// THE END. ALL THE BEST!